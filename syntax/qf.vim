highlight QuickFixError ctermbg=1,ctermfg=15
syntax match QuickFixError /\<error\>/

highlight QuickFixWarning ctermbg=3,ctermfg=15
syntax match QuickFixWarning /\<warning\>/
